Reference: https://github.com/clavearnel/philippines-region-province-citymun-brgy

Generate Philippines' province/citymun/brgy in one custom data structure

```bash
node generate.js >> phShippingAddresses.js

# generate.js returns a string structured like below
#
# {
#   province: [
#     'Abra',
#     'Benguet',
#     'Metro Manila~Quezon City',
#   ],
#   city: {
#     'Abra': ['Bangued', 'Dolores'],
#     'Benguet': ['La Trinidad'],
#     'Metro Manila~Quezon City': ['Quezon City'],
#   },
#   barangay: {
#     'Bangued': ['Agtangao', 'Banacao'],
#     'Dolores': ['Bayaan', 'Cabaroan', 'Calumbaya'],
#     'La Trinidad': ['Alapang', 'Alno', 'Ambiong'],
#     'Quezon City': ['Alicia', 'Amihan'],
#   }
# }
```