class LocalShippingAddresses {
  map = {
    province: {}, // { "provCode": "name" } }
    citymun: {}, // { "citymunCode": { "name", "provCode" } }
    barangay: {} // { "brgyCode": { "name", "cityMunCode" }
  }
  
  constructor () { }

  capitalize(str) {
    return str
      .toLowerCase()
      .replace(/\w\S*/g, (w) => w.replace(/^\w/, (c) => c.toUpperCase()))
  }

  populateProvinces(provinces) {
    provinces.forEach(({ provCode, provDesc }) => {
      this.map.province[provCode] = this.capitalize(provDesc)
    })
    return this
  }

  populateCityMun(cityMun) {
    cityMun.forEach(({ citymunCode, citymunDesc, provCode }) => {
      this.map.citymun[citymunCode] = {
        name: this.capitalize(citymunDesc),
        provCode,
      }
    })
    return this
  }

  populateBrgy(barangays) {
    barangays.forEach(({ brgyCode, brgyDesc, citymunCode }) => {
      this.map.barangay[brgyCode] = {
        name: this.capitalize(brgyDesc),
        citymunCode,
      }
    })
    return this
  }

  generateShippingAddressDataStructure({ sort = false }) {
    const { barangay, citymun, province } = this.map
    let ds = {
      province: [],
      city: {},
      barangay: {}
    }

    Object.keys(province).forEach(provCode => {
      const provName = province[provCode]
      ds.province.push(provName)
      ds.city[provName] = []
    })

    Object.keys(citymun).forEach(citymunCode => {
      const { name: cityName, provCode } = citymun[citymunCode]
      const provName = province[provCode]
      ds.city[provName].push(cityName)
      ds.barangay[cityName] = []
    })

    Object.keys(barangay).forEach(brgyCode => {
      const { name: brgyName, citymunCode } = barangay[brgyCode]
      const { name: cityName } = citymun[citymunCode]
      ds.barangay[cityName].push(brgyName)
    })

    if (sort) {
      ds.province.sort()
      Object.keys(ds.city).forEach(provName => ds.city[provName].sort())
      Object.keys(ds.barangay).forEach(cityName => ds.barangay[cityName].sort())
    }

    return JSON.stringify(ds)
  }
}

module.exports = LocalShippingAddresses