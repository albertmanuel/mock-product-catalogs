const LocalShippingAddresses = require('./LocalShippingAddresses')
const fs = require('fs')

const localShippingAddresses = new LocalShippingAddresses()
const PROVINCE_JSON_FILE = 'json/refprovince.json'
const CITYMUN_JSON_FILE = 'json/refcitymun.json'
const BRGY_JSON_FILE = 'json/refbrgy.json'

fs.readFile(PROVINCE_JSON_FILE, (err, data) => {
  if (err) console.error('Error', err.message)
  localShippingAddresses.populateProvinces(JSON.parse(data).RECORDS)

  fs.readFile(CITYMUN_JSON_FILE, (err, data) => {
    if (err) console.error('Error', err.message)
    localShippingAddresses.populateCityMun(JSON.parse(data).RECORDS)

    fs.readFile(BRGY_JSON_FILE, (err, data) => {
      if (err) console.error('Error', err.message)
      localShippingAddresses.populateBrgy(JSON.parse(data).RECORDS)

      console.log(localShippingAddresses.generateShippingAddressDataStructure({ sort: false }))
    })
  })
})
